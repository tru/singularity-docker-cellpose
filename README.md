# building an cellpose docker/singularity container

Tru <tru@pasteur.fr>

(toy) docker/singularity containers

Docker:
```
docker run -ti registry-gitlab.pasteur.fr/tru/singularity-docker-cellpose:main
```
Singularity:
```
singularity run oras://registry-gitlab.pasteur.fr/tru/singularity-docker-cellpose:latest
```

Dockefile is used to build the initial container, then singularity is used
to convert it to a OCI image, ready to be used.

## why ?
- working with gitlab CI as for github CI https://github.com/truatpasteurdotfr/cellpose
- just keep the required files for CI
